
import { Router, Route, Switch } from 'react-router-dom'
import Header from './components/Header'
import Home from './containers/Home'
import Footer from './components/Footer'
import './App.css'


function App() {
  return (
      <div className="App">
        <Header></Header>
          <Switch>
            <Route path='/'>
              <Home></Home>
            </Route>
          </Switch>
          <Footer></Footer>
      </div>
  );
}

export default App;
