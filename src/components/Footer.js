import { useEffect, useState } from "react"
import { Col, Container, Row } from "react-bootstrap"
import { FaSearch } from 'react-icons/fa'
import PamPokh from '../assets/images/pampokh-fa.svg'
import Logo from '../assets/images/logo.svg'
import Telegram from '../assets/images/telegram.svg'
import Twiter from '../assets/images/twiter.svg'
import Instagram from '../assets/images/instagram.svg'
import Facebook from '../assets/images/facebook.svg'
import Aparat from '../assets/images/aparat.svg'
import { Link } from "react-router-dom"

import './Footer.css'

const Footer = (props) => {
    const[search, setSearch] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()
        if(!search) {
            console.log('empty')
            return
        }
        console.log(search)
        setSearch('')
    }

    return(
        <Container fluid className="footer">
            <Row className="pt-4">
                <Col md={6}>
                    <form className="search-footer-form" onSubmit= {onSubmit}>
                        <button type="submit"><FaSearch></FaSearch></button>
                        <input name="search" placeholder="جستجو..." value={search} 
                        onChange={(e) => setSearch(e.target.value) }></input>
                    </form>
                </Col>
                <Col md={6}>
                    <div className="brand-footer">
                        <img src= {Logo} className="first-img" width="200px"></img>
                        <img src= {PamPokh} className="second-img" width="100px"></img>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h5 className="title-product-footer">محصولات</h5>
                    <div className="product-items">
                        <div>شلوار</div>
                        <div>تی شرت</div>
                        <div>تاپ</div>
                        <div>شلوارک</div>
                    </div>
                </Col>
                <Col>
                    <h5 className="text-fa-footer">پم پخ</h5>
                    <div className="website-items">
                        <div>
                            <Link to='#'>
                                قوانین و مقرارت
                            </Link>
                        </div>
                        <div>
                            <Link>
                                تماس با ما
                            </Link>
                        </div>
                        <div>
                            <Link>
                                بهترین دنبال کنندگان ما
                            </Link>
                            
                        </div>
                    </div>
                </Col>
                <Col className='m-auto'>
                    <div className = "footer-social-media">
                        <div>
                            <Link>
                                <img src = {Telegram}></img>
                            </Link>
                        </div>
                        <div>
                            <Link>
                                <img src={Twiter}></img>
                            </Link>
                        </div>
                        <div>
                            <Link>
                                <img src ={Instagram}></img>
                            </Link>
                        </div>
                        <div>
                            <Link>
                                <img src = {Facebook}></img>
                            </Link>
                        </div>
                        <div>
                            <Link>
                                <img src={Aparat}></img>
                            </Link>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}

export default Footer