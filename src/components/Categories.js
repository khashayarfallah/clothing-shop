
import { Container, Row , Col} from 'react-bootstrap'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Categories.css'
import Shalvar from '../assets/images/shalvar.svg'
import Tshirt from '../assets/images/tshirt.svg'
import Shalvarak from '../assets/images/shalvarak.svg'
import Shaal from '../assets/images/shaal.svg'
import Top from '../assets/images/top.svg'
import Jurab from '../assets/images/jurab.svg'
import { useState } from 'react';

const Categories = (props) => {
    const [category, setCategory] = useState('')

    return(
        <Container>
            <Row >
                <Col className="categories" onClick = {() => setCategory('shalvar')}>
                    <Link to='#' >
                        <img src={Shalvar} width = "35px"></img>
                    </Link>
                </Col> 
                <Col className="categories mr-1" onClick = {() => setCategory('tshirt')}>
                    <Link>
                        <img src={Tshirt} width = "70px"></img>
                    </Link>
                </Col>
                <Col className="categories mr-1" onClick = {() => setCategory('shalvarak')}>
                    <Link>
                        <img src={Shalvarak} width = "67px"></img>
                    </Link>
                </Col>
                <Col className="categories mr-1" onClick = {() => setCategory('top')}>
                    <Link>
                        <img src={Top} width="44px"></img>
                    </Link>
                </Col>
                <Col className="categories mr-1" onClick = {() => setCategory('shaal')}>
                    <Link>
                        <img src={Shaal} width="55px"></img>
                    </Link>
                </Col>
                <Col className="categories mr-1" onClick = {() => setCategory('jurab')}>
                    <Link>
                        <img src={Jurab} width="75px" className="pt-2"></img>
                    </Link>
                </Col>
            </Row>
        </Container>
    )
}

export default Categories